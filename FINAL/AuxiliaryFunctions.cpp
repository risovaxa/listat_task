#include "pch.h"
#include "AuxiliaryFunctions.h"


std::pair<std::vector<int>, std::vector<int>> AuxiliaryFunctions::convert(std::vector<std::string>&low, std::vector<std::string>&high)
{
	std::vector<int>collectionLow;
	std::vector<int>collectionHigh;

	if (low.size() != high.size())
		throw("wrong interval: check xml");

	
	for (int i = 1; i <= low.size(); i++)
	{
		try {
			int num = std::stoi(low[i - 1]);
			collectionLow.push_back(num);
			num = std::stoi(high[i - 1]);
			collectionHigh.push_back(num);
		}
		catch (...) { std::cerr << "Bad string" << "\n"; }
	}
	

	std::pair<std::vector<int>, std::vector<int>> rangeVector = make_pair(collectionLow, collectionHigh);
	return rangeVector;
}

