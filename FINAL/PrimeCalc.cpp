#include "pch.h"
#include "PrimeCalc.h"

void PrimeCalc::spawn()
{
	for (size_t i = 0; i < vectorLow.size(); i++) {
		vectorThread.push_back(std::thread(&PrimeCalc::primeCalc, this));
		vectorThread.at(i).join();
	}
}
std::vector<int> PrimeCalc::testPrime()
{
	for (size_t i = 0; i < vectorLow.size(); i++) {
		primeCalc();
	}
	return primes;
};

void PrimeCalc::primeCalc()
{
	int i, flag;
	while (vectorLow[index] < vectorHigh[index])
	{
		flag = 0;

		for (i = 2; i <= vectorLow[index] / 2; ++i)
		{
			if (vectorLow[index] % i == 0)
			{
				flag = 1;
				break;
			}
		}
		if (flag == 0)
			//std::cout << vectorLow[index] << "\n";
			primes.push_back(vectorLow[index]);
		vectorLow[index]++;

	}  
	
	while (vectorLow[index] > vectorHigh[index])
	{
			std::vector<int>newLow = vectorHigh;
			setHigh(vectorLow);
			setLow(vectorHigh);
			flag = 0;
			for (i = 2; i <= vectorLow[index] / 2; ++i)
			{
				if (vectorLow[index] % i == 0)
				{
					flag = 1;
					break;
				}
			}
			if (flag == 0)
				//std::cout << vectorLow[index] << "\n";
				primes.push_back(vectorLow[index]);
			vectorLow[index]++;
	}
	index++;
}
