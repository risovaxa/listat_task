#pragma once
#include <thread>
#include <string>
#include <vector>
#include <algorithm>
#include <exception>
#include "XmlParse.h"

class PrimeCalc
{
public:
	void spawn();
	
	std::vector<int> testPrime();
	
	void setPrimes(std::vector<int>prime_numbers) { primes = prime_numbers; }
	void setIndex(int vec_index) { index = vec_index; }
	void setLow(std::vector<int>low) { vectorLow = low; }
	void setHigh(std::vector<int>high) { vectorHigh = high; }

	std::vector<int> getPrimes() { return primes; }
	int getIndex() { return index; }

private:
	void primeCalc();
	std::vector<std::thread>vectorThread;
	std::vector<int>vectorLow;
	std::vector<int>vectorHigh;
	int index = 0;
	std::vector<int>primes;
};

