#include "pch.h"
#include "XmlParse.h"


std::string XmlParse::getFile() {
	std::string buffer;
	char c;
	std::ifstream in(filename);
	if (!in) {
		std::cerr << filename << " not found";   exit(1);
	}
	while (in.get(c))
		buffer += c;

	in.close();

	return buffer;
}

std::vector<std::string> XmlParse::getData(const std::string &text)
{
	std::vector<std::string> collection;
	size_t pos = 0, start;
	while (true)
	{
		start = text.find("<" + tag, pos);
		if (start == std::string::npos)
			return collection;
		start = text.find(">", start);
		start++;

		pos = text.find("</" + tag, start);
		if (pos == std::string::npos)
			return collection;
		collection.push_back(text.substr(start, pos - start));
	}
}

void XmlParse::setData(std::vector<int>primevec)
{
	std::ofstream os;
	os.open(filename);
	os << "<root>\n<primes>";
	for (int i = 0; i < primevec.size(); i++) {
		os << primevec[i] << " ";
	}
	os << "</primes>\n</root>";
	os.close();
}

void XmlParse::stripTags(std::string &text)
{
	unsigned int start = 0, pos;

	while (start < text.size())
	{
		start = text.find("<", start);
		if (start == std::string::npos) break;
		pos = text.find(">", start);
		if (pos == std::string::npos) break;
		text.erase(start, pos - start + 1);
	}
}
