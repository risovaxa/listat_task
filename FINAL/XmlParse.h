#pragma once
#include <string>
#include <vector>
#include <iostream>
#include <fstream>


class XmlParse
{
public:
	std::string getFile();

	std::vector<std::string> getData(const std::string &text); //get data by tag

	void setData(std::vector<int> primevec);
	void stripTags(std::string &text);

	void setFilename(std::string file_name) { filename = file_name; };
	void setTag(std::string file_tag) { tag = file_tag; };

	std::string getFilename() { return filename; };
	std::string getTag() { return tag; };

private:
	std::string filename;
	std::string tag = "high";
};

