#include "pch.h"
#include "XmlParse.h"
#include "AuxiliaryFunctions.h" 
#include "PrimeCalc.h"
#include <string>
#include <vector>
#include <thread>



int main()
{

	XmlParse parse;
	parse.setFilename("input.xml");
	bool stripOtherTags = true;


	//deserialize file
	std::string text = parse.getFile();

	//parsing by tag "low"
	parse.setTag("low");
	std::vector<std::string>lowString = parse.getData(text);

	//parsing by tag "high"
	parse.setTag("high");
	std::vector<std::string>highString = parse.getData(text);

	//converting from string vector to int vector
	AuxiliaryFunctions helper;
	auto ret = helper.convert(lowString, highString);
	std::vector<int>lowInt = ret.first;
	std::vector<int>highInt = ret.second;

	//threads
	PrimeCalc calc;
	calc.setLow(lowInt);
	calc.setHigh(highInt);
	calc.spawn();

	//writing primes in file
	std::vector<int> primes = calc.getPrimes();
	parse.setFilename("output.xml");
	parse.setData(primes);

	std::cout << "Job is done, check output.xml" << "\n";
	std::cout << "Press any key to exit" << "\n";
}

