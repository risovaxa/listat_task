#include "stdafx.h"
#include "CppUnitTest.h"
#include "..\FINAL\PrimeCalc.h"
#include "..\FINAL\XmlParse.h"
#include "..\FINAL\AuxiliaryFunctions.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace PrimeTest
{		
	TEST_CLASS(AppTest)
	{
	public:
		
		TEST_METHOD(getTagTest)
		{
			XmlParse parse;
			parse.setTag("high");
			std::string exp = "high";
			std::string act = parse.getTag();
			Assert::AreEqual(exp, act, L"Message", LINE_INFO());
		}
		
		TEST_METHOD(getPrimesTest)
		{
			PrimeCalc calc;
			std::vector<int>test = { 1,2,3 };
			
			int a = test[1];
			calc.setPrimes(test);
			std::vector<int>exp = test;
			std::vector<int>act = calc.getPrimes();
			int b = act[1];
			//we can not use vectors in Assert::AreEqual
			Assert::AreEqual(a, b, L"Message", LINE_INFO());
		}

	

	};
}